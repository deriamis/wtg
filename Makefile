ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME := "git-grove"

PKG := github.com/deriamis/$(PROJECT_NAME)

GO_BUILD_TAGS := static,system_libgit2
GO_BUILD_ARGS := -tags "$(GO_BUILD_TAGS)"
CGO_LDFLAGS_ALLOW := -R.*|-D_THREAD_SAFE

ifeq ($(OS),Windows_NT)
    detected_OS := Windows
else
    detected_OS := $(shell uname)
endif

STATIC_ROOT := $(ROOT_DIR)static-build/install
PKG_CONFIG_PATH := $(STATIC_ROOT)/lib/pkgconfig
ifeq ($(detected_OS),Darwin)
PKG_CONFIG_PATH := $(PKG_CONFIG_PATH):$(shell brew --prefix openssl@3)/lib/pkgconfig
endif

export PKG_CONFIG_PATH CGO_LDFLAGS_ALLOW

.PHONY: all build clean test coverage coverhtml lint

all: build-static-deps build test

lint: build ## Lint the files
	gosec -color -track-suppressions -stdout -fmt=json -out=gosec-results.json ./...
	staticcheck ./...
	golangci-lint run -v ./...

test: build ## Run unittests
	go test $(GO_BUILD_ARGS) -short ./...

race: build ## Run data race detector
	go test $(GO_BUILD_ARGS) -race -short ./...

msan: build ## Run memory sanitizer
	go test $(GO_BUILD_ARGS) -msan -short ./...

coverage: build ## Generate global code coverage report
	./tools/coverage.sh

coverhtml: build ## Generate global code coverage report in HTML
	./tools/coverage.sh html

build-static-libssh2-stamp:
	scripts/build-libssh2.sh
	touch $@

build-static-libgit2-stamp: build-static-libssh2-stamp
	scripts/build-libgit2.sh
	touch $@

build-static-deps: build-static-libgit2-stamp build-static-libssh2-stamp

build: build-static-deps ## Build the binary file
	go build $(GO_BUILD_ARGS) -v $(PKG)

install: build
	go install $(GO_BUILD_ARGS) -v $(PKG)

clean: ## Remove previous build
	rm -f "$(PROJECT_NAME)" coverage.html
	rm -rf static-build
	go clean -cache
	rm -f *-stamp

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
