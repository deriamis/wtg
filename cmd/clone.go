package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"net/url"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"strings"
	"syscall"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"

	"github.com/aristanetworks/goarista/monotime"
	"github.com/jdxcode/netrc"
	"github.com/kevinburke/ssh_config"
	git "github.com/libgit2/git2go/v34"
	"github.com/spf13/cobra"

	"github.com/deriamis/git-grove/internal/terminal"
	"github.com/deriamis/git-grove/internal/util"
)

var (
	quiet    bool
	verbose  bool
	progress bool
)

var cloneCmd = &cobra.Command{
	Use:          "clone [flags] repository [directory]",
	ValidArgs:    []string{"repository", "directory"},
	Short:        "Clone a bare git repository",
	Long:         "Clone a bare git repository",
	Args:         cobra.MinimumNArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		// Clean up git url

		type TransferState int
		const (
			TransferStateInit TransferState = iota
			TransferStateRecv
			TransferStateDone
			TransferStateIndexing
		)

		type AuthType int
		const (
			AuthTypeSSHAgent       AuthType = 1 << 0
			AuthTypeSSHKey         AuthType = 1 << 1
			AuthTypeSSHInteractive AuthType = 1 << 2
			AuthTypePlaintext      AuthType = 1 << 3
			AuthTypeDefault        AuthType = 1 << 4
		)

		var transferState TransferState
		var triedAuthTypes AuthType
		var lastIterTime uint64
		var clonePath string

		identityFiles := []string{
			"~/.ssh/id_rsa",
			"~/.ssh/id_ecdsa",
			"~/.ssh/id_ed25519",
		}

		lastSlashIndex := strings.LastIndex(args[0], "/")
		if lastSlashIndex == -1 {
			return errors.New("invalid repository URL format")
		}

		repoName := args[0][lastSlashIndex+1:]

		if len(args) > 1 {
			if rootDir != "" {
				clonePath = path.Join(rootDir, args[1])
			} else {
				clonePath = args[1]
			}
		} else {
			clonePath = repoName
		}

		remoteCallbacks := git.RemoteCallbacks{
			SidebandProgressCallback: func(str string) error {
				if !quiet || (terminal.InteractiveStdErr() && progress) {
					fmt.Fprintf(os.Stderr, "remote: %s", str)
				}
				return nil
			},
			CredentialsCallback: func(repo_url string, username string, allowed_types git.CredentialType) (credential *git.Credential, err error) {
				parsedUrl, err := url.Parse(repo_url)
				if err != nil {
					return nil, err
				}

				hostname := parsedUrl.Hostname()
				if strings.Contains(hostname, ":") {
					hostname = hostname[1 : len(hostname)-1]
				}

				currentUser, err := user.Current()
				if err != nil {
					return nil, err
				}

				netrcPath, err := util.GetAbsolutePath("~/.netrc")
				if err != nil {
					return nil, err
				}

				authFile, err := netrc.Parse(netrcPath)
				if err != nil {
					return nil, err
				}

				if username == "" && parsedUrl.Scheme == "https" {
					if authFile != nil && authFile.Machine(hostname) != nil {
						username = authFile.Machine(hostname).Get("username")
					}

					if username == "" {
						fmt.Fprintf(os.Stderr, "username: ")
						stdinScanner := bufio.NewScanner(os.Stdin)

						stdinScanner.Scan()
						username = string(stdinScanner.Text())
					}
				} else {
					username = ssh_config.Get(hostname, "User")

					if username == "" {
						username = os.Getenv("USER")
					}

					if username == "" {
						username = currentUser.Username
					}
				}

				if (allowed_types & git.CredentialTypeUsername) != 0 {
					return git.NewCredentialUsername(username)
				}

				if (triedAuthTypes & AuthTypeSSHKey) == 0 {
					if (allowed_types & git.CredentialTypeSSHKey) != 0 {

						if (triedAuthTypes & AuthTypeSSHAgent) == 0 {
							triedAuthTypes |= AuthTypeSSHAgent

							numAgentKeys := 0
							authSocket, agentErr := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK"))
							if agentErr == nil {
								defer authSocket.Close()
								client := agent.NewClient(authSocket)
								agentKeys, agentErr := client.List()
								if agentErr == nil {
									numAgentKeys = len(agentKeys)
								}
							}

							if numAgentKeys > 0 {
								return git.NewCredentialSSHKeyFromAgent(username)
							}
						}

						triedAuthTypes |= AuthTypeSSHKey

						if allowed_types&(git.CredentialTypeSSHMemory|git.CredentialTypeSSHCustom) != 0 {

							identityFile := ssh_config.Get(hostname, "IdentityFile")

							if identityFile != "" {
								identityFiles = append([]string{identityFile}, identityFiles...)
							}

							for _, identityFile = range identityFiles {
								privateKeyPath, err := util.GetAbsolutePath(identityFile)
								if err != nil {
									return nil, err
								}

								st, err := os.Stat(privateKeyPath)
								if err != nil {
									return nil, err
								}

								userOwnsFile := fmt.Sprint(st.Sys().(*syscall.Stat_t).Uid) == currentUser.Uid
								isUserReadable := (st.Mode().Perm() & 0o400) != 0
								isWorldAccessible := (st.Mode().Perm() & 0o077) != 0

								if userOwnsFile && isUserReadable && !isWorldAccessible {
									privateKeyBytes, err := os.ReadFile(filepath.Clean(privateKeyPath))
									if err != nil {
										return nil, err
									}

									var password string
									signer, err := ssh.ParsePrivateKey(privateKeyBytes)

									if errors.Is(err, &ssh.PassphraseMissingError{}) {
										password, err = terminal.ReadPassword()
										if err != nil {
											return nil, err
										}
									}

									publicKey := string(signer.PublicKey().Marshal()[:])
									privateKey := string(privateKeyBytes[:])

									return git.NewCredentialSSHKeyFromMemory(username, publicKey, privateKey, password)
								}
							}
						}
					}
				}

				if (allowed_types & git.CredentialTypeSSHInteractive) != 0 {
					password, err := terminal.ReadPassword()
					if err != nil {
						return nil, err
					}

					return git.NewCredentialUserpassPlaintext(username, password)
				}

				if (allowed_types & git.CredentialTypeUserpassPlaintext) != 0 {
					var password string

					if authFile != nil && authFile.Machine(hostname) != nil {
						password = authFile.Machine(hostname).Get("password")
					} else {
						password, err = terminal.ReadPassword()
						if err != nil {
							return nil, err
						}
					}

					return git.NewCredentialUserpassPlaintext(username, password)
				}

				return git.NewCredentialDefault()
			},
			TransferProgressCallback: func(stats git.TransferProgress) error {
				if quiet || (terminal.InteractiveStdErr() && !progress) {
					return nil
				}

				progress := stats.ReceivedObjects + stats.LocalObjects
				recvPercent := int(float64(progress) / float64(stats.TotalObjects) * float64(100))
				indexPercent := int(float64(stats.IndexedObjects) / float64(stats.TotalObjects) * float64(100))

				var recvRate float64
				if transferState == TransferStateInit {
					lastIterTime = monotime.Now()
					recvRate = float64(stats.ReceivedBytes)
				} else {
					elapsedTime := monotime.Since(lastIterTime)
					recvRate = float64(stats.ReceivedBytes) / float64(elapsedTime.Seconds())
				}

				transferMessage := fmt.Sprintf(
					"Receiving objects: %d%% (%d/%d), %s | %s/s",
					recvPercent,
					progress,
					stats.TotalObjects,
					util.HumanReadableBytes(stats.ReceivedBytes),
					util.HumanReadableBytes(recvRate))

				if progress != stats.TotalObjects {
					transferState = TransferStateRecv
					terminal.PrintMessage(transferMessage)
				} else {
					if transferState < TransferStateDone {
						transferState = TransferStateDone
					} else {
						if transferState != TransferStateIndexing {
							transferState = TransferStateIndexing
							fmt.Fprintf(os.Stderr, "%s, done\n", transferMessage)
						}

						message := fmt.Sprintf(
							"Indexing objects: %d%% (%d/%d)", indexPercent, stats.IndexedObjects, stats.TotalObjects)
						if stats.IndexedObjects == stats.TotalObjects {
							fmt.Fprintf(os.Stderr, "%s, done\n", message)
						} else {
							terminal.PrintMessage(message)
						}
					}
				}
				return nil
			},
			CertificateCheckCallback: func(cert *git.Certificate, valid bool, hostname string) error {
				if !valid {
					return git.GitError{
						Message: fmt.Sprintf("Invalid TLS certificate presented for host %s", hostname),
						Class:   git.ErrorClassSSL,
						Code:    git.ErrorCodeCertificate,
					}
				}
				return nil
			},
		}

		fetchOptions := git.FetchOptions{
			RemoteCallbacks: remoteCallbacks,
		}

		fmt.Printf("Cloning into bare repository '%s'...\n", clonePath)
		_, err := git.Clone(
			args[0],
			clonePath,
			&git.CloneOptions{
				Bare:         true,
				FetchOptions: fetchOptions,
			})
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	cloneCmd.PersistentFlags().BoolVarP(&quiet, "quiet", "q", false, "Operate quietly. Progress is not reported to the standard error stream.")
	cloneCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Run verbosely. Does not affect the reporting of progress status to the standard error stream.")
	cloneCmd.PersistentFlags().BoolVar(&progress, "progress", false, "Force progress status even if the standard error stream is not directed to a terminal.")
	rootCmd.AddCommand(cloneCmd)
}
