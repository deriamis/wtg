package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var rootDir string

var version = "DEV"

var rootCmd = &cobra.Command{
	Use:     "git-grove",
	Short:   "Git Worktree Management",
	Long:    `A tool for creating and managing git worktrees`,
	Version: version,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
