module github.com/deriamis/git-grove

go 1.20

require (
	github.com/aristanetworks/goarista v0.0.0-20230814185025-8653eb883b04
	github.com/jdxcode/netrc v1.0.0
	github.com/kevinburke/ssh_config v1.2.0
	github.com/libgit2/git2go/v34 v34.0.0
	github.com/spf13/cobra v1.7.0
	golang.org/x/crypto v0.12.0
	golang.org/x/term v0.12.0
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
