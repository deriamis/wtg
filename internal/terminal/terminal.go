package terminal

import (
	"bufio"
	"fmt"
	"os"
	"syscall"

	"golang.org/x/term"
)

func InteractiveStdin() bool {
	stdinFd, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	stdinIsCharDev := (stdinFd.Mode() & os.ModeCharDevice) != 0
	stdinIsPipe := (stdinFd.Mode() & os.ModeNamedPipe) != 0

	return !stdinIsCharDev || stdinIsPipe
}

func InteractiveStdErr() bool {
	stderrFd, err := os.Stderr.Stat()
	if err != nil {
		panic(err)
	}

	return stderrFd.Mode()&os.ModeNamedPipe == 0
}

func PrintMessage(message string) {
	if InteractiveStdErr() {
		fmt.Fprintf(os.Stderr, "%s\033[0K\r", message)
	} else {
		fmt.Fprintln(os.Stderr, message)
	}
}

func ReadPassword() (password string, err error) {
	if InteractiveStdin() {
		_, err = os.Stderr.WriteString("passphrase: ")
		if err != nil {
			return "", err
		}

		passwordBytes, err := term.ReadPassword(syscall.Stdin)
		defer fmt.Fprintln(os.Stderr)

		if err != nil {
			return "", err
		}

		password = string(passwordBytes[:])
	} else {
		stdinScanner := bufio.NewScanner(os.Stdin)
		stdinScanner.Scan()
		password = stdinScanner.Text()
	}

	return string(password), nil
}
