package util

import (
	"fmt"
	"os/user"
	"path/filepath"
)

func HumanReadableBytes[V float64 | int64 | uint](numBytes V) string {
	const unit = 1024

	if numBytes < unit {
		return fmt.Sprintf("%.0f B", float64(numBytes))
	}

	divisor, exponent := uint(unit), 0
	for n := numBytes / unit; n >= unit; n /= unit {
		divisor *= unit
		exponent++
	}

	return fmt.Sprintf(
		"%.2f %ciB",
		float64(numBytes)/float64(divisor),
		"KMGTPE"[exponent])
}

func GetAbsolutePath(path string) (string, error) {
	if len(path) != 0 && path[0] == '~' {

		usr, err := user.Current()
		if err != nil {
			return "", err
		}

		path = filepath.Join(usr.HomeDir, path[1:])
	}

	return filepath.Abs(path)
}
