package main

import (
	"github.com/deriamis/git-grove/cmd"
)

func main() {
	cmd.Execute()
}
