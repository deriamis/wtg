#!/bin/sh

set -e

ROOT=${ROOT-"$(cd "$(dirname "$0")/.." && echo "${PWD}")"}
VENDORED_PATH=${VENDORED_PATH-"${ROOT}/vendor/libgit2"}

BUILD_PATH="${ROOT}/static-build"
BUILD_INSTALL_PREFIX="${BUILD_PATH}/install"
mkdir -p "${BUILD_PATH}/install/lib"

mkdir -p "${BUILD_PATH}/build" &&
cd "${BUILD_PATH}/build" &&
cmake -DTHREADSAFE=ON \
      -DBUILD_CLI=OFF \
      -DBUILD_CLAR=OFF \
      -DBUILD_SHARED_LIBS=OFF \
      -DREGEX_BACKEND=builtin \
      -DUSE_BUNDLED_ZLIB=ON \
      -DUSE_HTTPS="OpenSSL-Dynamic" \
      -DUSE_SSH=ON \
      -DCMAKE_C_FLAGS=-fPIC \
      -DCMAKE_BUILD_TYPE="RelWithDebInfo" \
      -DCMAKE_INSTALL_PREFIX="${BUILD_INSTALL_PREFIX}" \
      -DCMAKE_INSTALL_LIBDIR="lib" \
      -DUSE_ICONV=OFF \
      -DUSE_NTLM_CLIENT=OFF \
      -DUSE_HTTP_PARSER=builtin \
      -DUSE_THREADS=ON \
      -DREGEX_BACKEND=builtin \
      -DBUILD_TESTS=OFF \
      -DBUILD_EXAMPLES=OFF \
      -DLIBSSH2_INCLUDE_DIRS="$(pkg-config libssh2 --variable=includedir)" \
      -DLIBSSH2_LIBRARY_DIRS="$(pkg-config libssh2 --variable=libdir)" \
      "${VENDORED_PATH}"

if which make nproc >/dev/null && [ -f Makefile ]; then
	# Make the build parallel if make is available and cmake used Makefiles.
	make "-j$(nproc --all)" install
else
	cmake --build . --target install
fi

rm -rfv "${BUILD_PATH}/build"
