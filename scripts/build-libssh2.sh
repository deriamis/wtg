#!/bin/sh

set -e

ROOT=${ROOT-"$(cd "$(dirname "$0")/.." && echo "${PWD}")"}
VENDORED_PATH=${VENDORED_PATH-"${ROOT}/vendor/libssh2"}

BUILD_PATH="${ROOT}/static-build"
BUILD_INSTALL_PREFIX="${BUILD_PATH}/install"
mkdir -p "${BUILD_PATH}/install/lib"

autoreconf -fi "${VENDORED_PATH}" &&
mkdir -p "${BUILD_PATH}/build" &&
cd "${BUILD_PATH}/build" &&
"${VENDORED_PATH}/configure" \
      --prefix "${BUILD_INSTALL_PREFIX}" \
      --enable-shared=no \
      --enable-static=yes \
      --disable-silent-rules \
      --disable-examples-build \
      --with-openssl \
      --with-libz \
      --with-libssl-prefix="$(pkg-config libssl --variable=prefix)" \
      --with-libz-prefix="$(pkg-config libz --variable-prefix)"

make "-j$(nproc --all)" install

rm -rfv "${BUILD_PATH}/build"
